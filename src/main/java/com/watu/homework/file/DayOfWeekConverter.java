package com.watu.homework.file;

import java.time.DayOfWeek;
import java.util.EnumSet;
import java.util.Optional;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

/**
 * Converter for opencsv library to convert integer into allowed days of week.
 * @author germans.kuzmins
 */
public class DayOfWeekConverter extends AbstractBeanField<EnumSet<DayOfWeek>, String> {
    private static final String TYPE_ERROR = "Can not parse provided value, please use integer in range [0, 127]";
    private static final String RANGE_ERROR = "Value is out of range, please use correct range [0, 127]";

    /**
     * Converting String value to allowed days of week.
     *
     * @param value represent a bitmask whenever day of week is included or not
     *              e.q. 0000001 only Monday, 0000101 Monday and Tuesday
     * @throws CsvDataTypeMismatchException if can not parse string to integer
     * @throws CsvConstraintViolationException if value outside allowed range [0, 127]
     */
    @Override
    protected EnumSet<DayOfWeek> convert(String value)
        throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        Integer bitmask = tryParse(value)
            .orElseThrow(() -> new CsvDataTypeMismatchException(value, Integer.class, TYPE_ERROR));

        if (bitmask < 0 || bitmask > 127) {
            throw new CsvConstraintViolationException(value, RANGE_ERROR);
        }

        EnumSet<DayOfWeek> days = EnumSet.noneOf(DayOfWeek.class);
        DayOfWeek[] values = DayOfWeek.values();
        for (int i = 0; i < values.length; i++) {
            if ((bitmask & (1 << i)) != 0) {
                days.add(values[i]);
            }
        }

        return days;
    }

    private Optional<Integer> tryParse(String value) {
        try {
            return Optional.of(Integer.parseInt(value));
        } catch (NumberFormatException ex) {
            return Optional.empty();
        }
    }

}

package com.watu.homework.file;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import com.opencsv.bean.CsvToBeanBuilder;

import lombok.extern.slf4j.Slf4j;

/**
 * Service which take file by path and convert to Task with time and allowed days of week.
 * @author germans.kuzmins
 */
@Service
@Slf4j
public class CsvFileProcessor {

    public List<Task> processCsvFile(String providedPath) {
        Path path = Paths.get(providedPath);
        try {
            if (Files.isDirectory(path)) {
                try (DirectoryStream<Path> stream = Files.newDirectoryStream(path, "*.csv")) {
                    return StreamSupport.stream(stream.spliterator(), false)
                        .flatMap(it -> readCSV(it).stream())
                        .toList();
                }
            } else {
                return readCSV(path);
            }
        } catch (IOException ex) {
            log.error("Error parsing Csv file!", ex);
            throw new IllegalStateException(ex);
        }
    }

    private List<Task> readCSV(Path pathToFile) {
        try(Reader reader = Files.newBufferedReader(pathToFile)) {
            return new CsvToBeanBuilder<Task>(reader)
                .withType(Task.class)
                .build()
                .parse();
        } catch (NoSuchFileException ex) {
            log.warn("Csv file not found for path {}", pathToFile.toAbsolutePath());
            return List.of();
        } catch (IOException ex) {
            log.error("Error parsing Csv file!", ex);
            throw new IllegalStateException(ex);
        }
    }
}

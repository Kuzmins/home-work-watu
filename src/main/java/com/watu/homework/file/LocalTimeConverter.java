package com.watu.homework.file;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.ResolverStyle;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

import lombok.extern.slf4j.Slf4j;

/**
 * Simple converter for opencsv library to convert String to LocalTime based on configured pattern.
 * @author germans.kuzmins
 */
@Slf4j
public class LocalTimeConverter extends AbstractBeanField<LocalTimeConverter, String> {
    private static final String TYPE_ERROR = "Can not parse time please provide time in HH:mm format e.g. 21:23";

    private final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
        .appendPattern("HH:mm")
        .toFormatter()
        .withResolverStyle(ResolverStyle.STRICT);


    @Override
    protected LocalTime convert(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        try {
            return LocalTime.parse(value, formatter);
        } catch (Exception ex) {
            log.error("Error parsing time value!", ex);
            throw new CsvDataTypeMismatchException(value, LocalTime.class, TYPE_ERROR);
        }
    }
}

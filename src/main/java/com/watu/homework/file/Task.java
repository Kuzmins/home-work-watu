package com.watu.homework.file;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.EnumSet;

import com.opencsv.bean.CsvCustomBindByName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    @CsvCustomBindByName(column = "time", converter = LocalTimeConverter.class)
    private LocalTime time;

    @CsvCustomBindByName(column = "bitmask", converter = DayOfWeekConverter.class)
    private EnumSet<DayOfWeek> daysOfWeek;
}


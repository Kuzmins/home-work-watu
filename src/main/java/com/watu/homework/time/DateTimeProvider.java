package com.watu.homework.time;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Service for providing current time in configured timezone and convert any other time in configured timezone
 * integration.default.timezone property is used for converting.
 * @author germans.kuzmins
 */
public class DateTimeProvider {
    private static Clock clock = Clock.systemUTC();
    private DateTimeProvider() {
    }

    /**
     * Used for DateTimeProvideConfiguration
     * @param timeZone Zone at which DateTimeProvider should work
     */
    public static void setTimeZone(String timeZone) {
        ZoneId zoneId = ZoneId.of(timeZone);
        clock = Clock.system(zoneId);
    }

    public static LocalDateTime nowAtClientZone() {
        return LocalDateTime.now(clock);
    }

    /** Function for testing used to invoke any other code with fixedTime
     * @param fixedTime fixed time
     * @param body code which will be invoked in scope of fixed time
     */
    public static void withFixedTime(Instant fixedTime, Runnable body) {
        Clock prevClock = clock;
        clock = Clock.fixed(fixedTime, prevClock.getZone());
        body.run();
        clock = prevClock;
    }
}

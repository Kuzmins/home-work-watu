package com.watu.homework.time;

import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import lombok.extern.slf4j.Slf4j;

/**
 * Configuration class for DateTimeProvider to set integration timezone from spring properties.
 * @author germans.kuzmins
 */
@Configuration
@Slf4j
public class DateTimeProvideConfig  {
    @Bean
    ApplicationRunner applicationRunner(Environment environment) {
        return args -> {
            String timeZone = environment.getProperty("default.business.timezone");
            log.info("Setting default integration timezone to {}", timeZone);
            DateTimeProvider.setTimeZone(timeZone);
        };
    }

}

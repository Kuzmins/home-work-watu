package com.watu.homework;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.watu.homework.file.CsvFileProcessor;
import com.watu.homework.file.Task;
import com.watu.homework.time.DateTimeProvider;

import lombok.extern.slf4j.Slf4j;

/**
 * Service which getting current business time, parse scv and invoke ActionService.
 * @author germans.kuzmins
 */
@Service
@Slf4j
public class SchedulerService {

    private final String pathToCsvFile;
    private final CsvFileProcessor csvFileProcessor;
    private final ActionService actionService;

    public SchedulerService(@Value("${path.to.file}") String pathToCsvFile, CsvFileProcessor csvFileProcessor,
                            ActionService actionService) {
        this.pathToCsvFile = pathToCsvFile;
        this.csvFileProcessor = csvFileProcessor;
        this.actionService = actionService;
    }

    @Scheduled(cron = "${scheduler.cron.expression}", zone = "${default.business.timezone}")
    public void checkTasks() {
        try {
            LocalDateTime currentBusinessTime = DateTimeProvider.nowAtClientZone();
            log.info("Starting task processing, local business time is {}", currentBusinessTime);
            List<Task> tasks = csvFileProcessor.processCsvFile(pathToCsvFile);
            tasks.forEach(task -> actionService.performActionForTask(task, currentBusinessTime));

        } catch (Exception ex) {
            log.error("Error processing task!", ex);
        }
    }

}

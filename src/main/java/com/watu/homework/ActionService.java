package com.watu.homework;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Service;

import com.watu.homework.file.Task;

import lombok.extern.slf4j.Slf4j;


/**
 * Simple service which takes task and determine if action should be invoked or not.
 * @author germans.kuzmins
 */
@Slf4j
@Service
public class ActionService {

    private final ThreadLocal<LocalDateTime> lastExecution = new ThreadLocal<>();

    public ActionService() {
        lastExecution.set(LocalDateTime.MIN);
    }

    public ActionResult performActionForTask(Task task, LocalDateTime currentBusinessTime) {
        if (!task.getDaysOfWeek().contains(currentBusinessTime.getDayOfWeek())) {
            log.info("Day of Week not matched for task, skipping");
            return ActionResult.SKIPPED;
        }

        if (task.getTime().getMinute() != currentBusinessTime.getMinute()) {
            LocalTime missMatchedTime = currentBusinessTime.truncatedTo(ChronoUnit.MINUTES).toLocalTime();
            log.info("Business time {} not match for task {}, skipping", missMatchedTime, task.getTime());
            return ActionResult.SKIPPED;
        }
        LocalDateTime lastExecutionTime = lastExecution.get();
        if (lastExecutionTime == null) {
            lastExecution.set(LocalDateTime.MIN);
            lastExecutionTime = LocalDateTime.MIN;
        }

        LocalDateTime dateTime1Truncated = lastExecutionTime.truncatedTo(ChronoUnit.MINUTES);
        LocalDateTime dateTime2Truncated = currentBusinessTime.truncatedTo(ChronoUnit.MINUTES);
        if (dateTime1Truncated.equals(dateTime2Truncated)) {
            log.info("Task was already executed skipping");
            return ActionResult.SKIPPED;
        }

        lastExecution.set(currentBusinessTime);
        runTask();
        return ActionResult.EXECUTED;
    }

    private void runTask() {
        // Custom logic for run task.
        log.info("Task was executed.");
    }

    public enum ActionResult {
        EXECUTED, SKIPPED
    }
}

package com.watu.homework;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.EnumSet;

import org.junit.jupiter.api.Test;

import com.watu.homework.ActionService.ActionResult;
import com.watu.homework.file.Task;

class ActionServiceTest {

    private final ActionService actionService = new ActionService();


    @Test
    void shouldPerformActionIfTimeAndDayInTaskMatch() {
        // Given:
        LocalTime time = LocalTime.parse("13:45");
        Task task = new Task(time, EnumSet.of(DayOfWeek.MONDAY));
        LocalDateTime currentTimeInAfrica = LocalDateTime.parse("2023-05-29T13:45:12.000");

        // When:
        ActionResult actionResult = actionService.performActionForTask(task, currentTimeInAfrica);
        // Then:
        assertThat(actionResult, is(ActionResult.EXECUTED));
    }

    @Test
    void shouldSkpActionIfTimeNotMatch() {
        // Given:
        LocalTime time = LocalTime.parse("13:45");
        Task task = new Task(time, EnumSet.of(DayOfWeek.MONDAY));
        LocalDateTime currentTimeInAfrica = LocalDateTime.parse("2023-05-29T13:46:00.000");

        // When:
        ActionResult actionResult = actionService.performActionForTask(task, currentTimeInAfrica);
        // Then:
        assertThat(actionResult, is(ActionResult.SKIPPED));
    }

    @Test
    void shouldSkipActionIfDayOfWeekNotMatched() {
        // Given:
        LocalTime time = LocalTime.parse("13:45");
        Task task = new Task(time, EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY));
        LocalDateTime currentTimeInAfrica = LocalDateTime.parse("2023-05-30T13:46:00.000");

        // When:
        ActionResult actionResult = actionService.performActionForTask(task, currentTimeInAfrica);
        // Then:
        assertThat(actionResult, is(ActionResult.SKIPPED));
    }

    @Test
    void shouldSkipActionIfWasAlreadyExecuted() {
        // Given:
        LocalTime time = LocalTime.parse("13:45");
        Task task = new Task(time, EnumSet.of(DayOfWeek.MONDAY));
        LocalDateTime currentTimeInAfrica = LocalDateTime.parse("2023-05-29T13:45:12.000");

        // When:
        ActionResult actionResult = actionService.performActionForTask(task, currentTimeInAfrica);

        // Then:
        assertThat(actionResult, is(ActionResult.EXECUTED));

        // When:
        currentTimeInAfrica = LocalDateTime.parse("2023-05-29T13:45:54.000");
        actionResult = actionService.performActionForTask(task, currentTimeInAfrica);

        // Then:
        assertThat(actionResult, is(ActionResult.SKIPPED));
    }
}
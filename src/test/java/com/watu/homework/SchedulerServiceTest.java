package com.watu.homework;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.EnumSet;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.watu.homework.file.CsvFileProcessor;
import com.watu.homework.file.Task;
import com.watu.homework.time.DateTimeProvider;

class SchedulerServiceTest {
    private final String pathToCsv = "some/path";
    private final CsvFileProcessor csvFileProcessor = mock(CsvFileProcessor.class);
    private final ActionService actionService = mock(ActionService.class);
    private final SchedulerService schedulerService = new SchedulerService(pathToCsv, csvFileProcessor, actionService);

    @BeforeAll
    static void beforeAll() {
        DateTimeProvider.setTimeZone("Africa/Lagos");
    }

    @Test
    void shouldParseCsvFileAndInvokeActionServiceWithCurrentBusinessTime() {
        // Given:
        Task task = new Task(LocalTime.parse("13:45"), EnumSet.allOf(DayOfWeek.class));
        List<Task> taskList = List.of(task);
        Instant fixedTime = Instant.parse("2022-05-29T12:45:01.234Z");

        // When:
        when(csvFileProcessor.processCsvFile(pathToCsv)).thenReturn(taskList);

        DateTimeProvider.withFixedTime(fixedTime, schedulerService::checkTasks);

        // Then:
        LocalDateTime expectedBusinessTime = LocalDateTime.parse("2022-05-29T13:45:01.234");
        verify(actionService).performActionForTask(task, expectedBusinessTime);
    }

    @Test
    void shouldNotInvokeServiceIfTasksWasNotProvided() {
        // When:
        when(csvFileProcessor.processCsvFile(pathToCsv)).thenReturn(List.of());
        schedulerService.checkTasks();

        // Then:
        verify(actionService, never()).performActionForTask(any(), any());
    }

}
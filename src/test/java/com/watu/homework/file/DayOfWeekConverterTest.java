package com.watu.homework.file;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

class DayOfWeekConverterTest {
    private final DayOfWeekConverter dayOfWeekConverter = new DayOfWeekConverter();

    @ParameterizedTest
    @CsvSource(
        value = {
            "1: MONDAY",
            "5: MONDAY, WEDNESDAY",
            "19: MONDAY, TUESDAY, FRIDAY",
            "64: SUNDAY",
            "127: MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY",
        }, delimiter = ':')
    void shouldConvertValueToProperDaysOfWeek(String input, String expected) throws Exception {
        // Given:
        EnumSet<DayOfWeek> expectedDays = Arrays.stream(expected.split(","))
            .map(it -> DayOfWeek.valueOf(it.trim().toUpperCase()))
            .collect(Collectors.toCollection(() -> EnumSet.noneOf(DayOfWeek.class)));

        // When:
        EnumSet<DayOfWeek> result = dayOfWeekConverter.convert(input);

        // Then:
        assertThat(result, is(expectedDays));
    }

    @ParameterizedTest
    @ValueSource(strings = {"-1", "128", "999999"})
    void shouldThrowExceptionIfValueExceedAllowedRange(String wrongValue) {
        // When:
        CsvConstraintViolationException exception = assertThrows(CsvConstraintViolationException.class, () -> {
            dayOfWeekConverter.convert(wrongValue);
        });

        //Then:
        assertThat(exception.getMessage(), is("Value is out of range, please use correct range [0, 127]"));
        assertThat(exception.getSourceObject(), is(wrongValue));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"String", " "})
    void shouldThrowExceptionIfValueCanNotConvertValue(String wrongValue) {
        // When:
        CsvDataTypeMismatchException exception = assertThrows(CsvDataTypeMismatchException.class, () -> {
            dayOfWeekConverter.convert(wrongValue);
        });

        //Then:
        assertThat(exception.getMessage(), is("Can not parse provided value, please use integer in range [0, 127]"));
        assertThat(exception.getSourceObject(), is(wrongValue));
        assertThat(exception.getDestinationClass(), is(Integer.class));
    }

    @Test
    void shouldReturnEmptyEnumSetIfNoDaysAreAllowed() throws Exception {
        // Given:
        String input = "0";

        // When:
        EnumSet<DayOfWeek> result = dayOfWeekConverter.convert(input);

        // Then:
        assertThat(result, hasSize(0));
    }
}
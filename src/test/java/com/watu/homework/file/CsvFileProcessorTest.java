package com.watu.homework.file;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.EnumSet;
import java.util.List;

import org.junit.jupiter.api.Test;

class CsvFileProcessorTest {
    CsvFileProcessor csvFileProcessor = new CsvFileProcessor();

    @Test
    void shouldParseCsvFileAndReturnTaskList() throws Exception {
        // Given:
        String pathToFile = getClass().getClassLoader().getResource("test_file.csv").toURI().getPath();

        // When:
        List<Task> result = csvFileProcessor.processCsvFile(pathToFile);

        // Then:
        assertThat(result, hasSize(2));

        Task firstTask = result.get(0);
        assertThat(firstTask.getTime(), is(LocalTime.parse("09:30")));
        assertThat(firstTask.getDaysOfWeek(), is(EnumSet.of(DayOfWeek.MONDAY)));

        Task secondTask = result.get(1);
        assertThat(secondTask.getTime(), is(LocalTime.parse("18:45")));
        assertThat(secondTask.getDaysOfWeek(), is(EnumSet.of(DayOfWeek.SUNDAY)));
    }

    @Test
    void shouldReturnEmptyListIfFileEmpty() throws Exception {
        // Given:
        String pathToFile = getClass().getClassLoader().getResource("empty_file.csv").toURI().getPath();

        // When:
        List<Task> result = csvFileProcessor.processCsvFile(pathToFile);

        // Then:
        assertThat(result, hasSize(0));
    }

    @Test
    void shouldReturnEmptyListIfFileContainOnlyHeaders() throws Exception {
        // Given:
        String pathToFile = getClass().getClassLoader().getResource("no_rows.csv").toURI().getPath();

        // When:
        List<Task> result = csvFileProcessor.processCsvFile(pathToFile);

        // Then:
        assertThat(result, hasSize(0));
    }

    @Test
    void shouldReturnEmptyListIfFileNotFound() {
        // Given:
        String pathToFile = "/not/existing/file";

        // When:
        List<Task> result = csvFileProcessor.processCsvFile(pathToFile);

        // Then:
        assertThat(result, hasSize(0));
    }

    @Test
    void shouldReadAllCsvFilesFromDirectoryAndConcatResult() throws Exception {
        // Given:
        String pathToFile = getClass().getClassLoader().getResource("directoryWithFiles").toURI().getPath();

        // When:
        List<Task> result = csvFileProcessor.processCsvFile(pathToFile);

        // Then:
        assertThat(result, hasSize(2));

        Task firstTask = result.get(0);
        assertThat(firstTask.getTime(), is(LocalTime.parse("09:30")));
        assertThat(firstTask.getDaysOfWeek(), is(EnumSet.of(DayOfWeek.MONDAY)));

        Task secondTask = result.get(1);
        assertThat(secondTask.getTime(), is(LocalTime.parse("18:45")));
        assertThat(secondTask.getDaysOfWeek(), is(EnumSet.of(DayOfWeek.SUNDAY)));
    }

}
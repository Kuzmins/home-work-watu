package com.watu.homework.file;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import com.opencsv.exceptions.CsvDataTypeMismatchException;

class LocalTimeConverterTest {
    private final LocalTimeConverter converter = new LocalTimeConverter();

    @ParameterizedTest
    @ValueSource(strings = {"00:00", "09:30", "17:30", "23:59"})
    void shouldConvertStringToLocalTime(String value) throws Exception {
        // When:
        LocalTime result = converter.convert(value);

        // Then:
        assertThat(result, is(LocalTime.parse(value)));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"24:00", "23:60", "someText"})
    void shouldThrowExceptionIfCanNotParseTime(String wrongValue) throws Exception {
        // When:
        CsvDataTypeMismatchException exception = assertThrows(CsvDataTypeMismatchException.class, () -> {
            converter.convert(wrongValue);
        });

        //Then:
        assertThat(exception.getMessage(), Matchers.is("Can not parse time please provide time in HH:mm format e.g. 21:23"));
        assertThat(exception.getSourceObject(), Matchers.is(wrongValue));
        assertThat(exception.getDestinationClass(), Matchers.is(LocalTime.class));
    }


}
package com.watu.homework;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
class HomeworkApplicationTests {

	@Autowired
	private Environment environment;

	@Test
	void shouldLoadContextAndSetDefaultValues() {
		assertThat(environment.getProperty("default.business.timezone"), is("Africa/Lagos"));
		assertThat(environment.getProperty("scheduler.cron.expression"), is("0 * * ? * *"));
		assertThat(environment.getProperty("path.to.file"), is("/some/test/path"));

	}

}

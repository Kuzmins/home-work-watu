# Simple App that parse

### Java 17 is required.

 Build app with gradlew:
```shell
./gradlew clean bootJar
```

Run built jar with command:
```shell
java -jar build/libs/homework-0.0.1-SNAPSHOT.jar
```
You can change configuration with -D flag
```shell
java -jar build/libs/homework-0.0.1-SNAPSHOT.jar -Dscheduler.cron.expression="0/15 * * ? * *" -Dpath.to.file="/Users/gkuzmins/Desktop/Watu/homework/src/test/resources/for_test.csv" -Ddefault.business.timezone="Africa/Lagos"
```
Or you can set env variables:

| Variable name             | Default value   |
|---------------------------|-----------------|
| PATH_TO_FILE              | /some/test/path |
| SCHEDULER_CRON_EXPRESSION | 0 * * ? * *     |
| DEFAULT_BUSINESS_TIMEZONE | Africa/Lagos    |

You can also use gradle wrapper to run application:
```shell
./gradlew clean bootRun -Dscheduler.cron.expression="0/15 * * ? * *" -Dpath.to.file="/Users/gkuzmins/Desktop/Watu/homework/src/test/resources/for_test.csv" -Ddefault.business.timezone="Africa/Lagos"
```
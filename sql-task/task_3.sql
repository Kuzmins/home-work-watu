/*
    Calculate current balance (scheduled amount - payed amount) for each loan.
    Use tables m_loan_repayment_schedule for payment schedule data.
    Total scheduled payment amount on current date must be calculated by sum of all amount field values.
    Some values can be null. Payment data are stored in table m_loan_transaction.
*/
SELECT
    s.loan_id,
    s.total_scheduled - COALESCE(t.total_transacted, 0) AS current_balance
FROM
    (SELECT
         loan_id,
         SUM(COALESCE(principal_amount, 0)) +
         SUM(COALESCE(interest_amount, 0)) +
         SUM(COALESCE(fee_charges_amount, 0)) +
         SUM(COALESCE(penalty_charges_amount, 0)) as total_scheduled
     FROM `loan-schema`.`m_loan_repayment_schedule`
     WHERE duedate <= CURDATE()
     GROUP BY loan_id) s
        LEFT JOIN
    (SELECT
         loan_id,
         SUM(amount) as total_transacted
     FROM `loan-schema`.`m_loan_transaction`
     WHERE transaction_date <= CURDATE()
     GROUP BY loan_id) t
    ON s.loan_id = t.loan_id;
/*
    Get numbers of vehicles sold (loans disbursed) in Jan and Feb 2020 per each vehicle make:
*/
SELECT `asset-schema`.`vehicle_make`.`name` AS vehicle_make,
       COUNT(*)                             AS num_vehicles_sold
FROM `loan-schema`.`m_loan`
         JOIN
     `asset-schema`.`asset`
     ON `loan-schema`.`m_loan`.`id` = `asset-schema`.`asset`.`m_loan_id`
         JOIN
     `asset-schema`.`vehicle_model`
     ON `asset-schema`.`asset`.`model_id` = `asset-schema`.`vehicle_model`.`id`
         JOIN
     `asset-schema`.`vehicle_make`
     ON `asset-schema`.`vehicle_model`.`vehicle_make_id` = `asset-schema`.`vehicle_make`.`id`
WHERE MONTH(`loan-schema`.m_loan.disbursedon_date) IN (1, 2)
  AND YEAR(`loan-schema`.`m_loan`.`disbursedon_date`) = 2020
GROUP BY vehicle_make;

/*
    Display only those makes where total sales are more than 1000 units
*/
SELECT `asset-schema`.`vehicle_make`.`name` AS vehicle_make,
       COUNT(*)                             AS num_vehicles_sold
FROM `loan-schema`.`m_loan`
         JOIN
     `asset-schema`.`asset`
     ON `loan-schema`.`m_loan`.`id` = `asset-schema`.`asset`.`m_loan_id`
         JOIN
     `asset-schema`.`vehicle_model`
     ON `asset-schema`.`asset`.`model_id` = `asset-schema`.`vehicle_model`.`id`
         JOIN
     `asset-schema`.`vehicle_make`
     ON `asset-schema`.`vehicle_model`.`vehicle_make_id` = `asset-schema`.`vehicle_make`.`id`
WHERE MONTH(`loan-schema`.m_loan.disbursedon_date) IN (1, 2)
  AND YEAR(`loan-schema`.`m_loan`.`disbursedon_date`) = 2020
GROUP BY vehicle_make
HAVING COUNT(*) > 2000;


/*
    Display full sales data including all makes from database
    (including those where sales are not made)
*/
SELECT vehicle_make.name AS vehicle_make, COUNT(`loan-schema`.`m_loan`.`id`) AS num_vehicles_sold
FROM `asset-schema`.`vehicle_make`
         LEFT JOIN `asset-schema`.`vehicle_model`
                   ON `asset-schema`.`vehicle_make`.`id` = `asset-schema`.`vehicle_model`.`vehicle_make_id`
         LEFT JOIN `asset-schema`.`asset` ON `asset-schema`.`vehicle_model`.`id` = `asset-schema`.`asset`.`model_id`
         LEFT JOIN `loan-schema`.`m_loan` ON `asset-schema`.`asset`.`m_loan_id` = `loan-schema`.`m_loan`.`id`
    AND MONTH(`loan-schema`.`m_loan`.`disbursedon_date`) IN (1, 2)
    AND YEAR(`loan-schema`.`m_loan`.`disbursedon_date`) = 2020
GROUP BY vehicle_make.name;

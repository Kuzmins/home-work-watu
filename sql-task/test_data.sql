
/*
    Generate vehicle make and models
*/
DROP PROCEDURE IF EXISTS generate_vehicles;

DELIMITER //
CREATE PROCEDURE generate_vehicles(IN numberOfModels INT)
BEGIN
    DECLARE i INT DEFAULT 1;
    DECLARE j INT DEFAULT 1;
    DECLARE makeId INT;

    -- Insert vehicle makes
    INSERT INTO `asset-schema`.`vehicle_make` (`name`) VALUES ('Toyota'), ('Ford'), ('Chevrolet'), ('Hyundai'),
                                                              ('Mercedes-Benz'), ('BMW'), ('Audi'),
                                                              ('Honda'), ('Nissan'), ('Volkswagen');

    -- Get the first insert id to accurately assign vehicle_make_id's to the models.
    SET @first_insert_id = LAST_INSERT_ID();

    WHILE i <= 10 DO
            SET makeId = @first_insert_id + i - 1;

            -- For each make, insert exactly numberOfModels
            SET j = 1;
            WHILE j <= numberOfModels DO
                    INSERT INTO `asset-schema`.`vehicle_model` (`vehicle_make_id`, `name`)
                    VALUES (makeId, CONCAT((SELECT `name` FROM `asset-schema`.`vehicle_make` WHERE `id` = makeId), ' Model ', j));

                    SET j = j + 1;
                END WHILE;

            SET i = i + 1;
        END WHILE;
END//
DELIMITER ;

CALL generate_vehicles(20);


/*
    Generate loans
*/
DROP PROCEDURE IF EXISTS generate_loans;

DELIMITER //
CREATE PROCEDURE generate_loans(IN fromMonth INT, IN toMonth INT, IN loansPerMonth INT)
BEGIN
    DECLARE i INT DEFAULT 1;
    DECLARE baseDate DATE DEFAULT '2019-11-01';
    DECLARE totalLoans INT;

    SET totalLoans = (toMonth - fromMonth + 1) * loansPerMonth;

    WHILE i <= totalLoans DO
            -- Generate 'loansPerMonth' loans for each month
            INSERT INTO `loan-schema`.`m_loan` (`account_no`, `disbursedon_date`)
            VALUES (CONCAT('loan', i), DATE_ADD(baseDate, INTERVAL fromMonth - 1 + FLOOR((i-1)/loansPerMonth) MONTH));

            SET i = i + 1;
        END WHILE;
END//
DELIMITER ;

-- Will generate loans from 2019-11-01 to 2020-04-01 2000 loans per each month
CALL generate_loans(1, 5, 100);


/*
    Generate assets
*/
DROP PROCEDURE IF EXISTS generate_assets;

DELIMITER //
CREATE PROCEDURE generate_assets()
BEGIN
    DECLARE i INT DEFAULT 0;
    DECLARE availableLoanId BIGINT;
    DECLARE randomModelId INT;
    DECLARE numberOfAssets INT;

    -- Count how many unassigned loans left
    SELECT COUNT(*) INTO numberOfAssets
    FROM `loan-schema`.`m_loan`
    WHERE `id` NOT IN (SELECT `m_loan_id` FROM `asset-schema`.`asset`);

    -- Create a temporary table to store model IDs
    DROP TEMPORARY TABLE IF EXISTS temp_model_ids;
    CREATE TEMPORARY TABLE temp_model_ids AS (SELECT `id` FROM `asset-schema`.`vehicle_model`);

    WHILE i < numberOfAssets DO
            -- Get first unassigned loan
            SELECT `id` INTO availableLoanId
            FROM `loan-schema`.`m_loan`
            WHERE `id` NOT IN (SELECT `m_loan_id` FROM `asset-schema`.`asset`)
            LIMIT 1;

            -- Get a random model
            SELECT `id` INTO randomModelId
            FROM temp_model_ids
            ORDER BY RAND()
            LIMIT 1;

            -- Insert into the assets table
            INSERT INTO `asset-schema`.`asset` (`model_id`, `registration_no`, `chassis_no`, `m_loan_id`)
            VALUES (randomModelId, CONCAT('REG', LPAD(i, 5, '0')), CONCAT('CHS', LPAD(i, 5, '0')), availableLoanId);

            SET i = i + 1;
        END WHILE;

    -- Drop the temporary table after use
    DROP TEMPORARY TABLE IF EXISTS temp_model_ids;
END//
DELIMITER ;


CALL generate_assets();

/*
    Generate weekly schedule
*/
DROP PROCEDURE IF EXISTS generate_weekly_schedule;

DELIMITER //
CREATE PROCEDURE generate_weekly_schedule()
BEGIN
    DECLARE loanId BIGINT;
    DECLARE disbursedDate DATE;
    DECLARE scheduleDueDate DATE;
    DECLARE i INT DEFAULT 1;
    DECLARE done INT DEFAULT FALSE;
    DECLARE principalAmount DECIMAL(19,6);
    DECLARE interestAmount DECIMAL(19,6);
    DECLARE feeChargesAmount DECIMAL(19,6);
    DECLARE penaltyChargesAmount DECIMAL(19,6);


    DECLARE loanCursor CURSOR FOR
        SELECT id, disbursedon_date FROM `loan-schema`.`m_loan`;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN loanCursor;

    read_loop: LOOP
        FETCH loanCursor INTO loanId, disbursedDate;
        IF done THEN
            LEAVE read_loop;
        END IF;

        SET scheduleDueDate = DATE_ADD(disbursedDate, INTERVAL 1 WEEK);
        SET principalAmount = ROUND((RAND() * (1000 - 500) + 500), 2);   -- Generate random value for principal_amount between 500 and 1000
        SET interestAmount = ROUND((RAND() * (100 - 10) + 10), 2); -- Generate random value for interest_amount between 10 and 100

        -- With 10% chance fee_charges_amount will be NULL
        IF FLOOR(1 + (RAND() * 10)) = 10 THEN
            SET feeChargesAmount = NULL;
        ELSE
            SET penaltyChargesAmount = ROUND((RAND() * (40 - 0) + 0), 2);   -- Generate random value for fee_charges_amount between 0 and 40
        END IF;

        -- With 10% chance penalty_charges_amount will be NULL
        IF FLOOR(1 + (RAND() * 10)) = 10 THEN
            SET penaltyChargesAmount = NULL;
        ELSE
            SET penaltyChargesAmount = ROUND((RAND() * (40 - 0) + 0), 2); -- Generate random value for penalty_charges_amount between 0 and 40
        END IF;


        WHILE i <= 8 DO
                INSERT INTO `loan-schema`.`m_loan_repayment_schedule` (
                    loan_id,
                    duedate,
                    principal_amount,
                    interest_amount,
                    fee_charges_amount,
                    penalty_charges_amount,
                    completed_derived
                )
                VALUES (
                           loanId,
                           scheduleDueDate,
                           principalAmount,
                           interestAmount,
                           feeChargesAmount,
                           penaltyChargesAmount,
                           0
                       );

                SET scheduleDueDate = DATE_ADD(scheduleDueDate, INTERVAL 1 WEEK);
                SET i = i + 1;
            END WHILE;

        SET i = 1;
    END LOOP;

    CLOSE loanCursor;
END//
DELIMITER ;

call generate_weekly_schedule();


/*
    Generate transactions
*/
DROP PROCEDURE IF EXISTS generate_loan_transactions;

DELIMITER //
CREATE PROCEDURE generate_loan_transactions()
BEGIN
    DECLARE currentLoanId BIGINT DEFAULT 0;
    DECLARE scheduleId BIGINT;
    DECLARE loanId BIGINT;
    DECLARE scheduleDueDate DATE;
    DECLARE principalAmount DECIMAL(19,6);
    DECLARE interestAmount DECIMAL(19,6);
    DECLARE feeChargesAmount DECIMAL(19,6);
    DECLARE penaltyChargesAmount DECIMAL(19,6);
    DECLARE totalAmount DECIMAL(19,6);
    DECLARE done INT DEFAULT FALSE;
    DECLARE skipRemaining INT DEFAULT 0;

    DECLARE scheduleCursor CURSOR FOR
        SELECT id, loan_id, duedate, principal_amount, interest_amount, fee_charges_amount, penalty_charges_amount
        FROM `loan-schema`.`m_loan_repayment_schedule`
        ORDER BY loan_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN scheduleCursor;

    read_loop: LOOP
        FETCH scheduleCursor INTO scheduleId, loanId, scheduleDueDate, principalAmount, interestAmount, feeChargesAmount, penaltyChargesAmount;
        IF done THEN
            LEAVE read_loop;
        END IF;

        IF loanId != currentLoanId THEN
            -- New loanId, reset skipRemaining
            SET currentLoanId = loanId;
            SET skipRemaining = 0;
        END IF;

        -- Skip current schedule if skipRemaining is 1
        IF skipRemaining = 1 THEN
            ITERATE read_loop;
        END IF;

        -- Decide whether to skip this transaction with a 10% chance
        IF FLOOR(RAND() * 10) = 0 THEN
            SET skipRemaining = 1;
            ITERATE read_loop;
        END IF;

        -- Replace any NULL values with 0
        SET principalAmount = COALESCE(principalAmount, 0);
        SET interestAmount = COALESCE(interestAmount, 0);
        SET feeChargesAmount = COALESCE(feeChargesAmount, 0);
        SET penaltyChargesAmount = COALESCE(penaltyChargesAmount, 0);

        SET totalAmount = principalAmount + interestAmount + feeChargesAmount + penaltyChargesAmount;

        -- Insert a single transaction for each loan schedule
        INSERT INTO `loan-schema`.`m_loan_transaction` (
            loan_id,
            is_reversed,
            transaction_date,
            amount
        )
        VALUES (
                   loanId,
                   0,
                   scheduleDueDate,
                   totalAmount
               );
    END LOOP;

    CLOSE scheduleCursor;
END//
DELIMITER ;

call generate_loan_transactions();
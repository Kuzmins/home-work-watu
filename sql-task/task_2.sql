/*
    Get current weekly payment amount for each loan.Table m_loan_repayment_schedule contains weekly payment records.
    Weekly payment record should be selected for the first week where obligations are not met (value for field completed_derived=0).
    Use principal_amount plus interest_amount to acquire weekly payment amount.
*/
    SELECT
    loan.id AS loan_id,
    loan.account_no,
    MIN(schedule.duedate) AS next_payment_date,
    schedule.principal_amount + schedule.interest_amount AS weekly_payment_amount
FROM
    `loan-schema`.`m_loan` AS loan
        INNER JOIN
    `loan-schema`.`m_loan_repayment_schedule` AS schedule ON loan.id = schedule.loan_id
WHERE
        schedule.completed_derived = 0
GROUP BY
    loan.id,
    loan.account_no,
    schedule.principal_amount,
    schedule.interest_amount;